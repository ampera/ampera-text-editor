# Ampera Text Editor

The Ampera Text Editor is an ncurses based text editor designed as an alternative to common terminal oriented text editors like nano, 
vi/vim, emacs, and mcedit. ATE is intended to mimic the style and design of old DOS based editors, with an emphasis on ease of use, 
and dead simple configuration. Features like full cursor support, high UI customizability including colour support, easy text 
highlighting definition, and much more can be expected.

ATE is written entirely in C++, and only uses one non-standard library (ncurses), meaning it can be easily compiled for most POSIX 
compatible platforms. The project is in its early stages, so the testing folder only has a Linux build present, and the build script 
is very rudimentary.
