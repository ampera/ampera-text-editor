#include <ncurses.h>
#include <string>
#include <iostream>
#include <signal.h>
#include <chrono>
#include <thread>


using namespace std;

string currentFile = "Untitled";
string displayedFileName = "";

bool isTermColour = false;
bool useColour = true;
bool useHiColours = false;
bool useUnicodeUI = false;
bool drawSides = true;

unsigned curPosX = 0;
unsigned curPosY = 0;

unsigned screenSizeX = 0;
unsigned screenSizeY = 0;

void drawFileName();
void drawColRow();
void setColours(bool useDefaults, bool useHiColor);
void drawBorder();
void redraw();
void drawMenuBar(bool hlFile, bool hlEdit);
void typeChar(wchar_t input);

int main(int argc, char **argv){
	//handle arguments
	for(int i = 1; i < argc; i++){
		std::string arg(argv[i]);
		if(arg.compare("-c") == 0 || arg.compare("--useHiColors") == 0 || arg.compare("--useHiColours") == 0){ //check if we are supposed to run using hi colour mode
			useHiColours = TRUE;
		}else if(arg.compare("-u") == 0 || arg.compare("--useUnicodeUI") == 0){
			useUnicodeUI = TRUE;
		}else if(arg.compare("-m") == 0 || arg.compare("--noColours") == 0 || arg.compare("--noColours") == 0){
                        useColour = FALSE;
                }else if(arg.compare("--disableSides") == 0){
			drawSides = false;
		}else if(arg.compare("-h") == 0 || arg.compare("--help") == 0){
		cout << "Ampera Text Editor Command Help\nUSAGE: ATE [SWITCHES]\n";
		cout << "-c, --useHiColours, --useHiColors 256 Colour support (broken on most terminals)\n";
		cout << "-m, --noColours, --noColors disable all colour support";
		cout << "-u, --useUnicodeUI Use unicode characters in UI (broken)\n";
		cout << "--disableSides disables side bars\n";
		cout << "-h, --help Shows this menu\n";
		return 0;
		}
	}


	initscr();
	setlocale(LC_ALL, "");

	if(has_colors() && useColour){ //do a check for colour support
		isTermColour = true;
	 	start_color();
		setColours(true, useHiColours);
	}

	raw();
	keypad(stdscr, TRUE);
	noecho();
	nodelay(stdscr, TRUE);

	getmaxyx(stdscr, screenSizeY, screenSizeX); //get screen size
	std::cout << screenSizeX << ' ' << screenSizeY;

	redraw();

	//start main loop
	while(1){
		unsigned newWinSizeX = 0;
		unsigned newWinSizeY = 0;
		wchar_t input = getch();

		getmaxyx(stdscr, newWinSizeY, newWinSizeX);
		refresh();
		if(newWinSizeX != screenSizeX || newWinSizeY != screenSizeY){ //keep checking if the screen size has changed, if so, redraw screen.
			getmaxyx(stdscr, screenSizeY, screenSizeX);
			redraw();
		}

		if(input >= 32 && input <= 126){
		typeChar(input);
		redraw();
		}

		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}

	return 0;
}

void typeChar(wchar_t input){
addwstr(&input);
}

void setColours(bool useDefaults, bool useHiColor){ //function to allocate the used colour pairs. These will be customizable by the end user.
	if(useDefaults){ //this is the hard coded default set of colours
		if(useHiColor){ //some terminals seem to not like just having 256 colours set, so this is for those that can like Alacritty
			init_pair(1, 226, 21); //Yellow on blue for UI
			init_pair(2, 15, 21); //White on blue for UI
			init_pair(3, 12, 21); //Light blue on blue for border tiles
			init_pair(4, 16, 249);
		}else{
			init_pair(1, COLOR_YELLOW, COLOR_BLUE);
			init_pair(2, COLOR_WHITE, COLOR_BLUE);
			init_pair(3, COLOR_CYAN, COLOR_BLUE);
			init_pair(4, COLOR_BLACK, COLOR_WHITE);
		}
	}

}

void redraw(){ //redraw the window
	string colRowText("Col: " + to_string(curPosX) + "Row: " + to_string(curPosY)); //use to find length of col row display

	if(screenSizeX < 9 + currentFile.length() + colRowText.length()){ //reduce the file name display's size by half if screen gets too small
		displayedFileName = currentFile.substr(0, currentFile.size() - currentFile.size() / 2);
	}else{
		displayedFileName = currentFile;
	}

	clear();
	if(screenSizeX > 8 + displayedFileName.length() + colRowText.length()  && screenSizeY > 6){
	drawBorder();
	drawMenuBar(false, false);
	move(2, 1);
	}else{
	mvaddstr(0, 0, "Terminal is too small!");
	}
	refresh();
}

void drawFileName(){
		addstr(displayedFileName.c_str());
}

void drawColRow(){

	if(isTermColour){
		attron(COLOR_PAIR(1));
		addstr("Col: ");

		attron(COLOR_PAIR(2));
		addstr(to_string(curPosX).c_str());
		addch(' ');

		attron(COLOR_PAIR(1));
		addstr("Row: ");

		attron(COLOR_PAIR(2));
		addstr(to_string(curPosY).c_str());
	}else{
		addstr("Col: ");
		addstr(to_string(curPosX).c_str());
		addch(' ');
		addstr("Row: ");
		addstr(to_string(curPosY).c_str());
	}
}

void drawBorder(){ //draw the border graphics


	string colRowText("Col: " + to_string(curPosX) + "Row: " + to_string(curPosY)); //use to find length of col row display
	if(useUnicodeUI){
			attron(COLOR_PAIR(3));
		if(drawSides){ //check if we are supposed to draw the sides of the screen
			for(int i = 1; i < screenSizeY - 1; i++){ //draw left side
				mvaddwstr(i, 0, L"\u2551");
			}
			for(int i = 1; i < screenSizeY - 1; i++){ //draw right side
				mvaddwstr(i, screenSizeX - 1, L"\u2551");
			}
		}
		if(screenSizeX < 50){
			mvaddwstr(screenSizeY - 1, 0, L"\u255A\u2550[ATE v0.01]");
			for(int i = 13; i < screenSizeX - 1; i++){ //draw bottom
				mvaddwstr(screenSizeY - 1, i, L"\u2550");
			}
		}else{
			mvaddwstr(screenSizeY - 1, 0, L"\u255A\u2550[Ampera Text Editor v0.01]");
			for(int i = 28; i < screenSizeX - 1; i++){ //draw bottom
	                        mvaddwstr(screenSizeY - 1, i, L"\u2550");
	                }
		}
	
		mvaddwstr(screenSizeY - 1, screenSizeX - 1, L"\u255D");
		mvaddwstr(0, 0, L"\u2554\u2550["); //start drawing top
		attron(COLOR_PAIR(1));
		drawFileName();
		attron(COLOR_PAIR(3));
		addch(']');
	
		for(int i = (displayedFileName.length() + 4); i < screenSizeX - (colRowText.length() + 5); i++){
			mvaddwstr(0, i, L"\u2550");
		}
		addch('[');
		drawColRow();
		attron(COLOR_PAIR(3));
		addch(']');
		attron(COLOR_PAIR(3));
		addwstr(L"\u2550\u2557");

		if(!drawSides){ //alter some UI elements 
			mvaddwstr(1, 0, L"\u255A");
			mvaddwstr(1, screenSizeX - 1, L"\u255D");
			mvaddwstr(screenSizeY - 1, 0, L"\u2554");
			mvaddwstr(screenSizeY - 1, screenSizeX - 1, L"\u2557");
		}
	}else{
	                attron(COLOR_PAIR(3));
		if(drawSides){ //check if we're supposed to draw sides
		        for(int i = 1; i < screenSizeY - 1; i++){ //draw left side
		                mvaddch(i, 0, ACS_VLINE);
		        }
		        for(int i = 1; i < screenSizeY - 1; i++){ //draw right side
		                mvaddch(i, screenSizeX - 1, ACS_VLINE);
		        }
		}
	        if(screenSizeX < 50){
			mvaddch(screenSizeY - 1, 0, ACS_LLCORNER);
			addch(ACS_HLINE);
	                addstr("[ATE v0.01]");
	                for(int i = 13; i < screenSizeX - 1; i++){ //draw bottom
	                        mvaddch(screenSizeY - 1, i, ACS_HLINE);
	                }
	        }else{
			mvaddch(screenSizeY - 1, 0, ACS_LLCORNER);
			addch(ACS_HLINE);
	                addstr("[Ampera Text Editor v0.01]");
	                for(int i = 28; i < screenSizeX - 1; i++){ //draw bottom
	                        mvaddch(screenSizeY - 1, i, ACS_HLINE);
	                }
	        }
	
	        mvaddch(screenSizeY - 1, screenSizeX - 1, ACS_LRCORNER);
		mvaddch(0, 0, ACS_ULCORNER); //start drawing top
		addch(ACS_HLINE);
		addch('[');
	        attron(COLOR_PAIR(1));
	        drawFileName();
	        attron(COLOR_PAIR(3));
	        addch(']');
	
	        for(int i = (displayedFileName.length() + 4); i < screenSizeX - (colRowText.length() + 5); i++){
	                mvaddch(0, i, ACS_HLINE);
	        }
	        addch('[');
	        drawColRow();
		attron(COLOR_PAIR(3));
	        addch(']');
	        attron(COLOR_PAIR(3));
		addch(ACS_HLINE);
		addch(ACS_URCORNER);
		if(!drawSides){ //alter some UI elements
                         mvaddch(1, 0, ACS_LLCORNER);
                         mvaddch(1, screenSizeX - 1, ACS_LRCORNER);
                         mvaddch(screenSizeY - 1, 0, ACS_ULCORNER);
                         mvaddch(screenSizeY - 1, screenSizeX - 1, ACS_URCORNER);
		}
	}
}

void drawMenuBar(bool hlFile, bool hlEdit){ //draw menu bar
	if(isTermColour){
		attron(COLOR_PAIR(4));
	}else{
		attron(A_REVERSE);
	}
	mvaddstr(1, 1, "  "); //add spacing before first menu item
	attron(A_UNDERLINE);
	addch('F'); //underlined F for File
	attroff(A_UNDERLINE);
	addstr("ile");
	addstr("  "); //space
	attron(A_UNDERLINE);
	addch('E'); //underlined E for Edit
	attroff(A_UNDERLINE);
	addstr("dit");

	for(int i = 12; i < screenSizeX - 2; i++){
	addch(' ');
	}
	attroff(A_REVERSE);

}

